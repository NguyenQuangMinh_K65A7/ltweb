<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Form Confirmation</title>
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
    <div class="container">
        <div id="errorMessages" class="error">
            <!-- Dùng để hiển thị lỗi nếu có -->
        </div>
        <form id="registrationForm" class="bor_der" method="POST" action="connect.php" enctype="multipart/form-data">
            <div class="form_gr">
                <div class="background text-white bor_der pd w-3 text-center required_label mg" for="name">Họ và tên
                </div>
                <?php
                echo $_POST['name'];
                ?>
                <input type="hidden" name="name" value="<?php echo $_POST['name']; ?>">
            </div>

            <div class="form_gr">
                <div class="background text-white bor_der pd w-3 text-center required_label mg " for="gender">Giới tính
                </div>
                <div id="gender" name="gender" class="w-1">
                    <?php
                    echo $_POST['gender'];
                    ?>
                </div>
                <input type="hidden" name="gender" value="<?php echo $_POST['gender']; ?>">
            </div>

            <div class="form_gr">
                <div class="background text-white bor_der pd w-3 text-center required_label mg" for="department">Phân
                    khoa</div>
                <?php
                $departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                echo $departments[$_POST['department']];
                // echo "<pre>";
                // echo print_r($_POST);
                ?>
                <input type="hidden" name="department" value="<?php echo $_POST['department']; ?>">
            </div>

            <div class="form_gr">
                <div class="background text-white bor_der pd w-3 text-center required_label mg w-1" for="birthdate">Ngày
                    sinh</div>
                <?php
                echo $_POST['birthdate'];
                ?>
                <input type="hidden" name="birthdate" value="<?php echo $_POST['birthdate']; ?>">
            </div>

            <div class="d-flex">
                <div class="background text-white bor_der pd w-3 text-center mg adress-label" for="address">Địa chỉ
                </div>
                <?php
                echo $_POST['address'];
                ?>
                <input type="hidden" name="address" value="<?php echo $_POST['address']; ?>">
            </div>

            <div class="d-flex">
                <div class="background text-white bor_der pd w-3 text-center mgin " for="Image" style="height: 10%">Hình
                    ảnh</div>

                <?php
                if (isset($_FILES['Image'])) {
                    $target_dir = 'uploads/';
                    $target_file = $target_dir . basename($_FILES['Image']['name']);
                    $maxFileSize = 84031 * 2;
                    $fileType = array('jpg', 'png', 'jpeg', 'gif');
                    $isImageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

                    if ($_FILES['Image']['size'] > $maxFileSize) {
                        echo '<div>Kích thước của ảnh quá lớn</div>';
                    } elseif (!in_array($isImageFileType, $fileType)) {
                        echo '<div>Sai định dạng ảnh</div>';
                    } elseif (move_uploaded_file($_FILES['Image']['tmp_name'], $target_file)) {
                        echo '<img src="./uploads/' . $_FILES["Image"]['name'] . '" alt="" style="width:50%; margin-left: 15px;">';
                    } else {
                        echo '<div>Không thể upload file</div>';
                    }
                }
                ?>
                <input type="hidden" name="image" value="<?php echo basename($_FILES["Image"]["name"]); ?>">
            </div>

            <div class="button-container" id="registerButton">
                <button type="submit">Xác nhận</button>
            </div>
        </form>
    </div>
</body>

</html>