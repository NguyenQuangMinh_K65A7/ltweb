<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <style>
        p {
            background-color: #f2f2f2;
            padding: 8px;
            width: 60%;
            margin-left: 1cm;
        }

        body {
            font-family: Arial, sans-serif;
            background-color: #f7f7f7;
            height: 100vh;
            margin: 0;
        }

        form {
            background-color: #fff;
            padding: 20px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            text-align: left;
            max-width: 45%;
            border: 2px solid #007bff;
        }

        label {
            font-weight: bold;
            background-color: #4682b4;
            padding: 10px;
            margin: 8px 0;
            color: white;
            display: inline-block;
            width: 20%;
            margin-right: 0.5cm;
            margin-left: 1cm;
        }

        input[type="text"] {
            width: 30%;
            padding: 10px;
            margin: 8px 0;
            border: 2px solid #007bff;
        }

        input[type="password"] {
            width: 30%;
            padding: 10px;
            margin: 8px 0;
            border: 2px solid #007bff;
        }

        input[type="submit"] {
            background-color: #4682B4;
            color: #fff;
            padding: 15px 35px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            font-weight: bold;
            margin-top: 10px;
            text-align: center;
            border: 2px solid #007bff;
            display: block;
            margin: 0 auto;
        }
    </style>
</head>

<body>
    <div>
        <form action="process_login.php" method="POST">
            <p>Bây giờ là:
                <?php
                $englishDays = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
                $vietnameseDays = array("Thứ 2 ngày", "Thứ 3 ngày", "Thứ 4 ngày", "Thứ 5 ngày", "Thứ 6 ngày", "Thứ 7 ngày", "Chủ Nhật ngày");

                $date = date("H:i:s, l d/m/Y", time() + 7 + 18000);

                $vietnameseDate = str_replace($englishDays, $vietnameseDays, $date);

                echo $vietnameseDate;
                ?>
            </p>

            <label for="username">Tên đăng nhập:</label>
            <input type="text" id="username" name="username" required><br><br>

            <label for="password">Mật khẩu:</label>
            <input type="password" id="password" name="password" required><br><br>

            <input type="submit" value="Đăng nhập">
        </form>
    </div>
</body>

</html>
