CREATE DATABASE ltweb;

USE ltweb;

CREATE TABLE students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    gender ENUM('Nam', 'Nữ') NOT NULL,
    department VARCHAR(255) NOT NULL,
    birthdate DATE NOT NULL,
    address VARCHAR(255),
    image VARCHAR(255)
);