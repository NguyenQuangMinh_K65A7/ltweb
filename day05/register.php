<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Đăng Ký</title>
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>

<style>
    /* Your CSS styles here */
</style>

<body>
    <div class="container">
        <form id="registrationForm" class="bor_der" method="POST" action="confirm.php" enctype="multipart/form-data">
            <div id="errorMessages" class="error">
            </div>

            <div class="form_gr">
                <div class="background text-white bor_der pd w-3 text-center required_label mg" for="name">Họ và tên</div>
                <input class="bor_der fl" type="text" id="name" name="name" required>
            </div>

            <div class="form_gr">
                <div class="background text-white bor_der pd w-3 text-center required_label mg " for="gender">Giới tính
                </div>
                <div id="gender" name="gender" class="w-1">
                    <input type="radio" id="male" name="gender" value="Nam" required> Nam
                    <input type="radio" id="female" name="gender" value="Nữ" required> Nữ
                </div>
            </div>

            <div class="form_gr">
                <div class="background text-white bor_der pd w-3 text-center required_label mg" for="department">Phân khoa
                </div>
                <select id="department" name="department" class="bor_der py">
                    <option value="">--Chọn phân khoa--</option>
                    <?php
                    $departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                    foreach ($departments as $key => $value) {
                        echo "<option value=\"$key\">$value</option>";
                    }
                    ?>
                </select>
            </div>

            <div class="form_gr">
                <div class="background text-white bor_der pd w-3 text-center required_label mg w-1" for="birthdate">Ngày sinh
                </div>
                <input class="bor_der w-1" type="text" id="birthdate" name="birthdate" placeholder="dd/mm/yyyy" required>
            </div>

            <div class="d-flex">
                <div class="background text-white bor_der pd w-3 text-center mg adress-label" for="address">Địa chỉ</div>
                <input class="bor_der fl address-input" type="text" id="address" name="address">
            </div>

            <div class="d-flex">
                <div class="background text-white bor_der pd w-3 text-center mgin" for="Image">Hình ảnh</div>
                <input class="file-input fle" type="file" id="Image" name="Image" accept="image/*">
            </div>

            <div class="button-container" id="registerButton">
                <button type="submit" name="submit">Đăng ký</button>
            </div>
        </form>
    </div>

    <script>
        $(function () {
            $("#registrationForm").submit(function (event) {
                var customErrorMessages = [];

                function addCustomErrorMessage(message) {
                    customErrorMessages.push(message);
                }

                var name = $("#name").val();
                var gender = $("input[name='gender']:checked").val();
                var department = $("#department").val();
                var birthdate = $("#birthdate").val();

                if (!name) {
                    addCustomErrorMessage("Hãy nhập tên.");
                }

                if (!gender) {
                    addCustomErrorMessage("Hãy chọn giới tính.");
                }

                if (!department) {
                    addCustomErrorMessage("Hãy chọn phân khoa.");
                }

                if (!birthdate) {
                    addCustomErrorMessage("Hãy nhập ngày sinh.");
                } else {
                    var datePattern = /^\d{2}\/\d{2}\/\d{4}$/;
                    if (!datePattern.test(birthdate)) {
                        addCustomErrorMessage("Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy.");
                    }
                }

                var errorMessageHtml = customErrorMessages.join("<br>");

                if (customErrorMessages.length > 0) {
                    $("#errorMessages").html(errorMessageHtml);
                    event.preventDefault();
                } else {
                    $("#errorMessages").html(""); // Clear error messages
                }
            });
        });
    </script>
</body>

</html>
