<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Đăng ký Tân sinh viên</title>
    <style>
        * {
            box-sizing: border-box;
            border: none;
            outline: unset;
            font-size: 18px;
            font-family: "Times New Roman", Times, serif;
        }

        body {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
        }

        form {
            width: 600px;
            padding: 60px 40px;
        }

        table {
            border-spacing: 10px;
        }

        .h-100 {
            height: 100%;
        }

        .d-flex {
            display: flex;
        }

        .py-10 {
            padding: 10px 0;
        }

        .mt-20 {
            margin-top: 20px;
        }

        .w-30 {
            width: 35%;
        }

        .w-100 {
            width: 100%;
        }

        .w-30 .present-time {
            background-color: rgb(234 229 229);
            margin: 0 0 20px 0;
        }

        .form-input input {
            padding: 20px 0 20px 10px;
        }

        .form-input input:hover {
            border-color: rgb(120, 217, 233);
        }

        .bg-blue {
            background-color: rgb(94 171 196);
        }

        .bg-green {
            background-color: rgb(86 170 70);
        }

        .text-white {
            color: white;
        }

        .text-center {
            text-align: center;
        }

        .bd-blue {
            border: 2px solid rgb(5 84 149);
        }

        .pass-input {
            margin-bottom: 50px;
        }

        .btn {
            padding: 12px 40px;
            border-radius: 10px;
            cursor: pointer;
        }

        .btn:hover {
            background-color: rgb(135, 211, 237);
        }

        .p-10-20 {
            padding: 10px 20px;
        }

        .genders {
            margin-right: 10px;
        }

        .genders input {
            margin-right: 10px;
        }
    </style>
</head>

<body>
    <form action="" class="bd-blue">
        <table class="w-100">
            <tr>
                <td class="bg-blue text-white bd-blue p-10-20 w-30 text-center"><label for="name">Họ và tên</label></td>
                <td class="bd-blue">
                    <input type="text" id="name" name="name" required class=" w-100 p-10-20 h-100">
                </td>
            </tr>
            <tr>
                <td class="bg-blue text-white bd-blue p-10-20 w-30 text-center"><label for="gender">Giới tính</label>
                </td>
                <td>
                    <div class="d-flex">
                        <?php
                        $genders = array(0 => 'Nam', 1 => 'Nữ');
                        for ($i = 0; $i < count($genders); $i++) {
                            $key = $i;
                            $value = $genders[$i];

                            echo "<div class='genders'>
                            <input id='$key' type='radio' name='genders' value='$value'>
                            <label for='$key'>$value</label>
                        </div>";
                        }
                        ?>
                    </div>

                </td>

            </tr>
            <tr>
                <td class="bg-blue text-white bd-blue p-10-20 w-30 text-center"><label for="department">Phân
                        khoa</label></td>
                <td>

                    <select id="department" name="department" class="bd-blue py-10">
                        <option value="">--Chọn phân khoa--</option>
                        <?php
                        $departments = array('Khoa học máy tính', 'Khoa học vật liệu');
                        foreach ($departments as $department) {
                            echo "<option value=\"$department\">$department</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>
        </table>
        <div class="text-center ">
            <button type="submit" class="btn bd-blue bg-green text-white mt-20">Đăng ký</button>
        </div>
    </form>
</body>

</html>