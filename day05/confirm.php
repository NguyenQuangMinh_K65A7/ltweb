<!DOCTYPE html>
<html lang="en">

<head>
    <title>Form Validation</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<?php
$departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
?>

<body>
    <div class="container">
        <form id="registrationForm" class="bd-blue" method="POST" enctype="multipart/form-data">
        <div class="form-group">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center required-label me-20" for="name">Họ và
                    tên</div>

                <div class="fl-1 p-10-20">
                    <?php
                    echo $_POST['name'];
                    ?>
                </div>
        </div>

            <div class="form-group">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center required-label me-20 " for="gender">
                    Giới tính</div>
                <div id="gender" name="gender" class="fl-1 p-10-20">
                        <?php
                        echo $_POST['gender'];
                        ?>
                </div>
            </div>

            <div class="form-group">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center required-label me-20" for="department">
                    Phân khoa</div>
                <div class="fl-1 p-10-20">
                    <?php
                    echo $departments[$_POST['department']];
                    ?>
                </div>
            </div>

            <div class="form-group">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center required-label me-20 w-170"
                    for="birthdate">Ngày sinh</div>
                <div class="fl-1 p-10-20">
                    <?php
                    echo $_POST['birthdate'];
                    ?>
                </div>
            </div>

            <div class="form-group">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center me-20 w-170"
                    for="address">Địa chỉ</div>
                <div class="fl-1 p-10-20">
                    <?php
                    echo $_POST['address'];
                    ?>
                </div>
            </div>

            <div class="form-group" style="align-items: unset">
                <div class="bg-green text-white bd-blue p-10-20 w-30 text-center me-20"
                    for="profileImage" style="height: 20%">Hình ảnh</div>
                <div class="p-0-20 w-30">
                <?php
                if (isset($_FILES['profileImage'])) {
                    if ($_FILES['profileImage']['error'] == 0) {
                        $target_dir = 'uploads/';
                        $target_file = $target_dir . basename($_FILES['profileImage']['name']);
                        $uploads = true;
                        $maxFileSize = 84031 * 2;
                        $fileType = array('jpg', 'png', 'jpeg', 'gif');
                        $isImageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                        if (file_exists($target_file)) {
                            echo '<div>File da ton tai</div>';
                            $uploads = false;
                        }

                        if ($_FILES['profileImage']['size'] > $maxFileSize) {
                            echo '<div>kich thuoc cua anh qua lon</div>';
                            $uploads = false;
                        }

                        if (!in_array($isImageFileType, $fileType)) {
                            echo '<div>sai dinh dang anh</div>';
                            $uploads = false;
                        }
                        if ($uploads) {
                            if (move_uploaded_file($_FILES['profileImage']['tmp_name'], $target_file)) {
                                echo '<img src="./uploads/' . $_FILES["profileImage"]['name'] . '" alt="" style="width:100%">';
                            } else {
                                echo 'khong upload dc file';
                            }
                        }

                    }
                }
                ?>

                </div>
                
            </div>

            <div class="button-container" id="registerButton">
                <button type="submit">Xác nhận</button>
            </div>
        </form>
    </div>
</body>

</html>
