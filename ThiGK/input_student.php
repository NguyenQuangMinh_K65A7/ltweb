<!DOCTYPE html>
<html>
<head>
  <title>Form Đăng Kí Sinh Viên</title>
  <style>
    body {
      font-family: Arial, sans-serif;
      background-color: #f0f0f0;
    }

    h1 {
      text-align: center;
      font-size: 24px;
    }

    form {
      width: 60%;
      margin: 0 auto;
      background-color: #fff;
      padding: 20px;
      border: 1px solid #ccc;
      border-radius: 5px;
    }

    .form-group {
      display: flex;
      flex-direction: row;
      align-items: center;
      margin: 10px 0;
      border-bottom: 1px solid #ccc;
      padding: 10px 0;
    }

    .form-group label {
      flex: 1;
      font-size: 18px;
    }

    .form-control {
      flex: 2;
    }

    .small-select {
      width: 100px;
      margin-right: 5px;
    }

    input[type="text"],
    input[type="radio"],
    select,
    textarea {
      width: 100%;
      padding: 8px;
      margin-top: 5px;
      border: 1px solid #ccc;
      border-radius: 3px;
      font-size: 16px;
    }

    select {
      height: 36px;
    }

    .error {
      color: red;
      font-size: 14px;
    }

    #thanhPho, #quan {
      width: 50%;
    }

    #thongTinKhac {
      height: 100px;
    }

    input[type="button"] {
  display: block;
  margin: 10px auto; 
  padding: 10px;
  background-color: #007bff;
  color: #fff;
  border: none;
  border-radius: 3px;
  font-size: 18px;
  cursor: pointer;
}
  </style>
</head>
<body>
<h1>Form Đăng Kí Sinh Viên</h1>'
  <form id="studentForm" action="submit_student.php" method="post">
  <span id="hoTenError" class="error"></span>
  <span id="gioiTinhError" class="error"></span>
  <span id="ngaySinhError" class="error"></span>
  <span id="diaChiError" class="error"></span> 
  <form id="studentForm" action="submit_student.php" method="post">
    <div class="form-group">
      <label>Họ và tên:</label>
      <div class="form-control">
        <input type="text" id="hoTen" name "hoTen" value="">
      </div>
    </div>

    <div class="form-group">
      <label>Giới tính:</label>
      <div class="form-control">
        <input type="radio" id="nam" name="gioiTinh" value="1">
        <label for="nam">Nam</label>
        <input type="radio" id="nu" name="gioiTinh" value="2">
        <label for="nu">Nữ</label>
      </div>
    </div>

    <div class="form-group">
  <label>Ngày sinh:</label>
  <select id="namSinh" name="namSinh">
    <option value="" selected disabled>Chọn năm</option>
    <?php
    for ($year = 2008; $year >= 1983; $year--) {
      echo "<option value='$year'>$year</option>";
    }
    ?>
  </select>
  <select id="thangSinh" name="thangSinh">
    <option value="" selected disabled>Chọn tháng</option>
    <?php
    for ($month = 1; $month <= 12; $month++) {
      echo "<option value='$month'>$month</option>";
    }
    ?>
  </select>
  <select id="ngaySinh" name="ngaySinh">
    <option value="" selected disabled>Chọn ngày</option>
    <?php
    for ($day = 1; $day <= 31; $day++) {
      echo "<option value='$day'>$day</option>";
    }
    ?>
  </select>
</div>


    <div class="form-group">
      <label>Địa chỉ:</label>
      <div class="form-control">
        Thành phố:
        <select class="small-select" id="thanhPho" name="thanhPho">
          <option value="" selected disabled>Chọn thành phố</option>
          <option value="hanoi">Hà Nội</option>
          <option value="hochiminh">Tp. Hồ Chí Minh</option>
        </select>
        Quận:
        <select class="small-select" id="quan" name="quan">
          <option value="" selected disabled>Chọn quận</option>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label>Thông tin khác:</label>
      <div class="form-control">
        <textarea id="thongTinKhac" name="thongTinKhac"></textarea>
      </div>
    </div>
    <input type="button" value="Đăng Ký" onclick="validateForm()">
  </form>

  <script>
  function validateForm() {
    var hoTen = document.getElementById("hoTen").value;
    var gioiTinh = document.querySelector("input[name='gioiTinh']:checked");
    var namSinh = document.getElementById("namSinh").value;
    var thangSinh = document.getElementById("thangSinh").value;
    var ngaySinh = document.getElementById("ngaySinh").value;
    var thanhPho = document.getElementById("thanhPho").value;
    var quan = document.getElementById("quan").value;

    var hoTenError = document.getElementById("hoTenError");
    var gioiTinhError = document.getElementById("gioiTinhError");
    var ngaySinhError = document.getElementById("ngaySinhError");
    var diaChiError = document.getElementById("diaChiError");

    hoTenError.innerHTML = "";
    gioiTinhError.innerHTML = "";
    ngaySinhError.innerHTML = "";
    diaChiError.innerHTML = "";

    if (hoTen.trim() === "") {
      hoTenError.innerHTML = "Hãy nhập họ tên";
    }

    if (!gioiTinh) {
      gioiTinhError.innerHTML = "Hãy nhập giới tính";
    }

    if (namSinh === "" || thangSinh === "" || ngaySinh === "") {
      ngaySinhError.innerHTML = "Hãy nhập ngày sinh";
    }

    if (thanhPho === "" || quan === "") {
      diaChiError.innerHTML = "Hãy nhập địa chỉ";
    }
  }

  var thanhPhoSelect = document.getElementById("thanhPho");
  var quanSelect = document.getElementById("quan");

  thanhPhoSelect.addEventListener("change", function () {
    quanSelect.innerHTML = "";

    if (thanhPhoSelect.value === "hanoi") {
      var hanoiQuan = ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"];
      for (var i = 0; i < hanoiQuan.length; i++) {
        var option = document.createElement("option");
        option.text = hanoiQuan[i];
        quanSelect.add(option);
      }
    } else if (thanhPhoSelect.value === "hochiminh") {
      var hcmQuan = ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"];
      for (var j = 0; j < hcmQuan.length; j++) {
        var option = document.createElement("option");
        option.text = hcmQuan[j];
        quanSelect.add(option);
      }
    }
  });
</script>
</body>
</html>